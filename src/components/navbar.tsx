import Link from 'next/link'
import React from 'react'

const Navbar = () => {
  return (
    <nav className='border-2 flexBetween max-container padding-container relative z-30 py-5'>
        <ul className='hidden h-full gap-12 lg:flex'>
            <li> <Link className="regular-16 text-gray flexCenter cursor-pointer pb-1.5 transition-all hover:font-bold" href="/">Home</Link> </li>
            <li> <Link className="regular-16 text-gray flexCenter cursor-pointer pb-1.5 transition-all hover:font-bold" href="/artists_page">Top 10 Artists</Link> </li>
            <li> <Link className="regular-16 text-gray flexCenter cursor-pointer pb-1.5 transition-all hover:font-bold" href="/tracks_page">Top 10 Tracks</Link> </li>
        </ul>
    </nav>
  )
}

export default Navbar