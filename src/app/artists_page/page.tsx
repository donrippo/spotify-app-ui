"use client"

import React, {useEffect, useState} from "react";


export default function index() {
    const [artistNames, setArtistNames] = useState([])
    const [artistGenres, setArtistGenres] = useState([])
    const [error, setError] = useState(null);
    useEffect(() => {fetch("http://localhost:5000/get_top_artists")
        .then((response) => response.json())
        .then((data) => 
        {
          setArtistNames(data.artist_name); 
          setArtistGenres(data.genre_0);
        }).catch(error => {
          setError(error)});
        },[]);

    if (error) return (<p>{error}</p>)
  
    return (
     <div className="outer-container"> 
      <div className="main-content">
        <h1>Top 10 Artists</h1>
        <table className="main-table">
          <tr>
            <th className="table-first-column">#</th>
            <th>Artist names</th>
            <th>Artist's primary genre</th>
          </tr>
          {artistNames.map((_, idx) => 
          <tr>
            <td className="table-first-column">{idx}</td>
            <td>{artistNames[idx]}</td>
            <td>{artistGenres[idx]}</td>
          </tr>)}
        </table>
      </div>
    </div>);
};
