"use client"

import React, {useEffect, useRef, useState} from "react";
import sampleTracksJson from "../../../utils/test_artists.json";
import "./page.css";

const ITEM_WIDTH = 200;



export default function page() {
  type CardItem = {
    artist_name: string;
    image_link: string;
  }
  
  const CardItem = ({artist_name, image_link}:CardItem) => {
    return (<div className="card" style={{backgroundImage:'url('+image_link+')'}}>
      <p>{artist_name}</p>
    </div>)
  }

  const [scrollPosition, setScrollPosition] = useState(0);
  const containerRef = useRef();

  const artist_names = sampleTracksJson["artist_name"]
  const album_cover_links = sampleTracksJson["album_cover_link"]

  const handleScroll = (scrollAmount) => {
    const newScrollPosition = scrollPosition + scrollAmount;
    setScrollPosition(newScrollPosition);
    containerRef.current.scrollLeft = newScrollPosition;
  }

  return <div className="container">
    <div ref={containerRef} style={{width:"900px", overflowX:"scroll",scrollBehavior:"smooth"}}>
      <div className="content-box">
      {artist_names.map((name, index) =><CardItem artist_name={name} image_link={album_cover_links[index]} />)}
      </div>
    </div>
    <div className="action-btns">
      <button onClick={() => handleScroll(-ITEM_WIDTH)}>Scroll Left</button>
      <button onClick={() => handleScroll(ITEM_WIDTH)}>Scroll Right</button>
    </div>
  </div>
}



function check_response(response:Response) {
  return (typeof response !== undefined)
}

function index() {
    const [trackNames, setTrackNames] = useState([])
    const [artistNames, setArtistNames] = useState([])
    const [albumNames, setAlbumNames] = useState([])
    const [albumReleaseDates, setAlbumReleaseDates] = useState([])
    const [error, setError] = useState(null);

    useEffect(() => {fetch("http://localhost:5000/get_top_tracks")
        .then((response) => response.json())
        .then((data) => 
        {
          setTrackNames(data.track_name); 
          setArtistNames(data.artist_name);
          setAlbumNames(data.album_name);
          setAlbumReleaseDates(data.album_release_date);
          console.log(data.album_release_date)
          console.log("Got results")
        }).catch(error => {
          console.log(error)
          setError(error)});
        },[]);

    if (error) return (<p>{error}</p>);

    if (typeof trackNames === undefined) return(
    <div className="outer-container"> 
          <div className="main-content">
            <h1>Top 10 Artists</h1>
            <table className="main-table">
              <tr>
                <th className="table-first-column">#</th>
                <th>Track names</th>
                <th>Primary artist</th>
                <th>Album name</th>
                <th>Album release date</th>
              </tr>
              <tr>
                <td className="table-first-column">No data fetched</td>
                <td>No data fetched</td>
                <td>No data fetched</td>
                <td>No data fetched</td>
                <td>No data fetched</td>
              </tr>
            </table>
          </div>
        </div>
    )
    
    return (
     <div className="outer-container"> 
      <div className="main-content">
        <h1>Top 10 Tracks</h1>
        <table className="main-table">
          <tr>
            <th className="table-first-column">#</th>
            <th>Track names</th>
            <th>Primary artist</th>
            <th>Album name</th>
            <th>Album release date</th>
          </tr>
          {
          trackNames.map((_, idx) => 
          <tr>
            <td className="table-first-column">{idx}</td>
            <td>{trackNames[idx]}</td>
            <td>{artistNames[idx]}</td>
            <td>{albumNames[idx]}</td>
            <td>{albumReleaseDates[idx]}</td>
          </tr>)}
        </table>
      </div>
    </div>);
};